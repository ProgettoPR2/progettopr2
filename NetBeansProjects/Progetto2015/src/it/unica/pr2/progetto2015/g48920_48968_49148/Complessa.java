/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48920_48968_49148;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;

/**
 * 
 * @author Federica Muceli 
 * @author Federica Gerina
 * @author Flavia Murru
 * 
 */

public class Complessa implements SheetFunction
{
        @Override
        public Object execute(Object... args)
	{		
		Integer[][] matrix=(Integer[][]) args;
		int n;

		if(matrix.length==matrix[0].length) //Matrice quadrata
		{

			Integer app;
			for (int i=0; i<matrix.length; i++)
			{
				for(int j=i+1; j<matrix[0].length; j++)
				{
					app=matrix[i][j];
					matrix[i][j]=matrix[j][i];
					matrix[j][i]=app;
				}	
			}
			return matrix;
		}
			
                else //Matrice rettangolare
		{
                        //Matrice con dimensioni invertite rispetto a quella di partenza
			Integer[][] trasposta = new Integer[matrix[0].length][matrix.length];
		
			for(int i=0; i<matrix[0].length; i++)
			{
				for(int j=0; j<matrix.length; j++)
				{
					trasposta[i][j]= matrix[j][i];
				}
			}
			return trasposta;
			
		}	
	}
        
        @Override
        public String getCategory()
	{
		return "Matrice";
	}

        @Override
	public String getHelp()
	{
		return "Traspone le righe e le colonne di una matrice.";
	}

        @Override
	public String getName()
	{
		return "MATR.TRASPOSTA";
	}
}

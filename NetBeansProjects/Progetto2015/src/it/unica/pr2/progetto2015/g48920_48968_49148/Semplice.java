/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48920_48968_49148;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;

/**
 * 
 * @author Federica Muceli 
 * @author Federica Gerina
 * @author Flavia Murru
 * 
 */

public class Semplice implements SheetFunction
{
	@Override
        public Object execute(Object... args)
	{
		Integer numero;	
		if (args[0] instanceof Integer) 
		{	
			numero=(Integer) args[0];			
			int i;
    			Integer f=1;
   			for(i=1; i<=numero; i=i+1) {
      				f=f*i;
			}
			return f;
			
		}
		else
			return "Errore nell'inserimento del parametro";
        }

        @Override
	public String getCategory()
	{
		return "Matematica";
	}

        @Override
	public String getHelp()
	{
		return "Restituisce il fattoriale di un numero.";
	}

        @Override
	public String getName()
	{
		return "FATTORIALE";
	}	
	
}

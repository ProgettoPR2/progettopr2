/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unica.pr2.progetto2015.g48920_48968_49148;
import facebook4j.*;
import facebook4j.auth.AccessToken;
import it.unica.pr2.progetto2015.interfacce.SheetFunction;
import java.util.ArrayList;


/**
 * 
 * @author Federica Muceli 
 * @author Federica Gerina
 * @author Flavia Murru
 * 
 */

public class Custom implements SheetFunction
{
    @Override
    public Object execute(Object ...args)
    {
        //Token di accesso
        final String MY_APP_ID="493321410832355";
        final String MY_APP_SECRET="466a7b815d94825e284670c69b85c2be";
        final String accessToken="CAAHArFdz5ZBMBANX5IeK06EODHJoWwadv5UPzRbnwb5jcBOMHZCZCN5z35kn2D5wbZB5CYyMnxaPOxXzEJPHrproZAAvn2p3A9HsAqeZCh8dzHjpQ3bIKNv1dZATXY03vsIHpZAJYVeyXqZBe7BBimaSy22SDbeq8PZBHopZAamZBZBZBXlB1PY4n1o6xlFT1UgEYLVCsZD";
        
        //Inizializzazione applicazione
        Facebook facebook=new FacebookFactory().getInstance();
        facebook.setOAuthAppId(MY_APP_ID, MY_APP_SECRET);
        facebook.setOAuthAccessToken(new AccessToken(accessToken));
        
        //Scelta dell'utente
        Integer scelta=(Integer) args[0];
        
        try
        {
             //Inizializzazione profilo
             User user;
             user=facebook.getMe();
             
             switch(scelta)
             {
                case 1: //Nome 
                    return user.getName();
                case 2: //Compleanno
                    return user.getBirthday();
                case 3: //Su di me
                    return user.getBio();
                case 4: //URL profilo
                    return user.getLink();
                case 5: //Email
                    return user.getEmail();
                case 6: //Lista degli eventi a cui partecipo
                    //Salvo gli eventi in un'apposita lista
                    ResponseList<Event> listEvent;
                    listEvent=facebook.getEvents();
                    //Salvo il nome degli eventi in un'altra lista che verrà restituita dal metodo
                    ArrayList<String> listNomi=new ArrayList<String>();
                    for(Event event:listEvent)
                        listNomi.add(event.getName());
                    return listNomi;
                case 7: //Post pubblicati
                    //Salvo i post in un'apposita lista
                    ResponseList<Post> listPost;
                    listPost=facebook.getPosts();
                    //Salvo il messaggio correlato al post in un'altra lista che verrà restituita dal metodo
                    ArrayList<String> listNomiPost=new ArrayList<String>();
                    for(Post post:listPost)
                        /*Se il post non e' un messaggio (es: aggiornamento dell'immagine del profilo)
                        aggiungo alla lista la descrizione del post, se non e' presente nemmeno quella aggiungo l'URL del post*/
                        if(post.getMessage()==null)
                            listNomiPost.add(post.getLink().toString());
                        else
                            listNomiPost.add(post.getMessage());
                    return listNomiPost;
                       
                
                default: return "Il numero inserito non corrisponde a nessuna opzione. Riprova";
            }
        }catch(FacebookException e)
        { 
            return "Attenzione. Errore nell'accedere a Facebook";
        }
    }
    
    @Override
    public String getCategory()
    {
            return "Facebook Application";
    }
       

    @Override
    public String getHelp()
    {
        return "Restituisce informazioni su un profilo facebook";
    }

   
    @Override
    public String getName()
    {
        return "Custom";
        
    }   
        
}

# README #

# Esame: 
Programmazione 2

# Titolo: 
Progetto 2015

# Autori:

Flavia		Murru		48920;
Federica	Muceli		48968;
Federica 	Gerina		49148;

	
# Descrizione:

Il progetto consiste nell'implementazione, in linguaggio Java, di due funzioni LibreOffice e di una funzione Custom scelta da noi e implementata grazie all'utilizzo di librerie esterne.
Le funzioni poi vengono eseguite su un foglio di calcolo LibreOffice (foglio.ods) grazie all'estensione Obba 4.2.2.

Le funzioni di LibreOffice scelte sono quella per il calcolo del fattoriale (FATTORIALE) quella per il calcolo di una matrice trasposta (MATR_TRASPOSTA).
Le funzioni FATTORIALE e MATR_TRASPOSTA sono state calcolate sul foglio elettronico, sia dalla funzione apposita di LibreOffice, che dalla funzione da noi implementata.

La funzione Custom è un'applicazione Facebook che restituisce informazioni su un profilo. Per implementare questa funzione è stata utilizzata la libreria esterna facebook4j (http://facebook4j.org/javadoc/index.html).
Le informazioni che possono essere restituite sono elencate in un menù scritto sul foglio elettronico apposito (foglio.ods), e sono: nome del profilo, compleanno, informazioni sul profilo ("Su Di Me"), il link che riconduce al profilo, e-mail, gli eventi al quale l'utente ha partecipato e quelli a cui intende partecipare, e i post pubblicati. La scelta viene operata inserendo il numero dell'opzione nell'apposita cella (N.B: Il menù può essere testato senza dover chiamare la funzione per ogni sua voce).

La funzione Facebook, per essere testata, richiede la connessione ad Internet.

E' possibile testarla con un profilo creato appositamente:

E-mail: *pr2progetto@gmail.com*;
Password: *progettopr2*

Per creare l'applicazione è stata necessaria la registrazione sul sito https://developers.facebook.com/ per ottenere delle credenziali indispensabili all'implementazione della stessa (APP_ID, APP_SECRET, ACCESS_TOKEN) e per attivare le autorizzazioni di accesso ad alcuni dati personali. E' possibile accedere a suddette credenziali tramite l'e-mail e la password precedentemente indicate.